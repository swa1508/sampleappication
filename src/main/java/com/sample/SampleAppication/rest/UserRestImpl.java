package com.sample.SampleAppication.rest;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sample.SampleAppication.model.User;
import com.sample.SampleAppication.service.IUserService;

@RestController
@RequestMapping("/user")
public class UserRestImpl {
	
	private Logger logger=LogManager.getLogger(UserRestImpl.class);
	
	@Autowired
	private IUserService userService;
	
	@RequestMapping("/hello")
	public String showMsg() {
		logger.info("Inside @class UserRestImpl @method showMsg");
		return "Hello From Sample Application";
	}
	
	@PostMapping("/create")
	public User save(@RequestBody User user) {
		logger.info("Inside @class UserRestImpl @method saveUser {}",user.toString());
		return userService.save(user);	
	}
	
	@GetMapping("/search")
	public List<User> search(){
		return userService.search();
	}
	
	@GetMapping("/searchRecordCount")
	public Long getSearchRecordCount() {
		return userService.getSearchRecordCount();		
	}
	
	@GetMapping("/findById/{id}")
	public Optional<User> findById(@PathVariable (value = "id") Long id) {
        return userService.findById(id);
    }
	
	@GetMapping("/deleteById/{id}")
	public void deleteById(@PathVariable (value = "id") Long id) {
        userService.deleteById(id);
    }
	
	@PutMapping("/update/{id}")
	public User update(@PathVariable(value = "id") Long id,@RequestBody User user) {
		return userService.update(id,user);
	}
	
	
	

}
