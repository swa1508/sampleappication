package com.sample.SampleAppication.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sample.SampleAppication.model.User;
import com.sample.SampleAppication.repository.UserRepository;
import com.sample.SampleAppication.service.IUserService;

@Service
public class UserServiceImpl implements IUserService {
	
	private Logger logger=LogManager.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public User save(User user) {
		logger.info("inside @class UserServiceImpl @method saveUser{}",user.toString());
		return userRepository.save(user);
	}

	@Override
	public List<User> search() {
		return userRepository.findAll();
	}

	@Override
	public Long getSearchRecordCount() {
		return userRepository.count();
	}

	@Override
	public Optional<User> findById(Long id) {
		return userRepository.findById(id);
	}

	@Override
	public void deleteById(Long id) {
		userRepository.deleteById(id);;
	}

	@Override
	public User update(Long id, User user) {
		User userDb = userRepository.findById(id).get();
		userDb.setFirstName(user.getFirstName());
		userDb.setMiddleName(user.getMiddleName());
		userDb.setLastName(user.getLastName());
		userDb.setEmail(user.getEmail());
		userDb.setMobile(user.getMobile());
		userDb.setAddress(user.getAddress());
		return userRepository.save(userDb);
	}

}
