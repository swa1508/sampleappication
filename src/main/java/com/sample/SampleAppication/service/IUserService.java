package com.sample.SampleAppication.service;

import java.util.List;
import java.util.Optional;

import com.sample.SampleAppication.model.User;

public interface IUserService {
	
	public User save(User user);

	public List<User> search();

	public Long getSearchRecordCount();

	public Optional<User> findById(Long id);

	public void deleteById(Long id);

	public User update(Long id, User user);

}
