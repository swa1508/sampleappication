package com.sample.SampleAppication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sample.SampleAppication.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {


}
