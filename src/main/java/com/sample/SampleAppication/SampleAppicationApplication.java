package com.sample.SampleAppication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleAppicationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleAppicationApplication.class, args);
	}

}
